﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using SiteELearning.Models;
using System.Collections;


namespace SiteELearning.Controllers
{
    public class StudentController : Controller
    {
        List<niveauscolair> ls = new List<niveauscolair>();
        protected linqtosql1DataContext db;
        // GET: Student
        public ActionResult Index(Student user)
        {
            if(user.Id > 0)
            {
                db = new linqtosql1DataContext();
                List<Cour> cours = (from c in db.Cours where c.idNiv == user.idNiv select c).ToList<Cour>();
                return View(cours);
            }
            else
            {
                return RedirectToAction("loginStudent");
            }
            
        }

        //------------------------------------------------------------------------------
        //login

        public ActionResult loginStudent() {
            Student value = new Student();
            return View();
        }
        [HttpPost]
        public ActionResult loginStudent(Student value)
        {
            db = new linqtosql1DataContext();
            Student user = (from u in db.Students where u.username == value.username && u.password == value.password select u).SingleOrDefault<Student>();
            if(user != null)
            {
                if(user.firstTime == true)
                {
                    return RedirectToAction("changeInfo",user);
                }
                else
                {
                    return RedirectToAction("Index",user);
                }
            }
            else
            {
                return RedirectToAction("Inscription");
            }
        }

        //-----------------------------------------------------------------
        //inscription

        public List<niveauscolair> nivScolair()
        {
            db = new linqtosql1DataContext();
            var niv = (from nv in db.niveauscolairs select nv).ToList();
            return niv;
        }

        public ActionResult Inscription()
        {
            ls = nivScolair();
            Student value = new Student();
            ViewBag.list = ls;
            return View();
        }
        [HttpPost]
        public ActionResult Inscription(FormCollection objfrm, Student value)
        {
            db = new linqtosql1DataContext();
            List<Student> l = new List<Student>();
            string un = value.nom.Substring(0, 4) + value.prenom.Substring(0,4);
            var rand = new Random();
            string pw = un.Substring(2, 4) + value.email.Substring(0, 5) + rand.Next(1000, 9999);

            l.Add(new Student() { nom = value.nom, prenom = value.prenom, email = value.email, username = un, password = pw, idNiv = value.idNiv, firstTime=true});

            db.Students.InsertAllOnSubmit(l);
            db.SubmitChanges();
            //if (email(value, un, pw))
            //{
            //    ViewBag.mailStatus = "Message sent";
            //    return RedirectToAction("loginStudent");
            //}
            //else
            //{
            //    ViewBag.mailStatus = "Message not sent";
            //    return View();
            //
            return RedirectToAction("loginStudent");
        }
        public bool email(Student value, string username, string pwuser)
        {
            try
            {
                string subject = "UserName et password pour la prochaine connection";
                string message = "Bonjour voici le username : " + username + " et voici le password : " + pwuser;
                string MailSender = System.Configuration.ConfigurationManager.AppSettings["MailSender"].ToString();
                string pw = System.Configuration.ConfigurationManager.AppSettings["MailPw"].ToString();

                SmtpClient smtpClient = new SmtpClient("smtp.office365.com", 587);

                smtpClient.EnableSsl = true;
                smtpClient.Timeout = 100000;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(MailSender, pw);

                MailMessage mailMessage = new MailMessage(MailSender, value.email, subject, message);

                mailMessage.BodyEncoding = System.Text.UTF8Encoding.UTF8;

                smtpClient.Send(mailMessage);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        //------------------------------------------------------------------------------
        //change info

        public ActionResult changeInfo()
        {

            return View();
        }
        [HttpPost]
        public ActionResult changeInfo(Student user)
        {
            db = new linqtosql1DataContext();
            Student st = (from u in db.Students where u.nom == user.nom && u.email == user.email select u).SingleOrDefault<Student>();

            if (st != null)
            {
                st.username = user.username;
                st.password = user.password;
                st.firstTime = false;
                db.SubmitChanges();
                return RedirectToAction("Index",st);
            }
            else
            {
                return RedirectToAction("loginStudent");
            }
        }

    }
}